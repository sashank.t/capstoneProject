// // import React, { useState } from 'react';
// // import axios from 'axios';

// // const RegistrationForm = () => {
// //     const [formData, setFormData] = useState({
// //         name: '',
// //         gender: '',
// //         email: '',
// //         dob: '',
// //         mobileNumber: '',
// //         givenId: ''
// //     });

// //     const handleChange = (e) => {
// //         setFormData({
// //             ...formData,
// //             [e.target.name]: e.target.value
// //         });
// //     };

// //     const handleSubmit = async (e) => {
// //         e.preventDefault();
// //         try {
// //             const response = await axios.post('/api/userRoutes/register', formData);
// //             console.log('User registered successfully:', response.data);
// //         } catch (error) {
// //             console.error('Error registering user:', error.response ? error.response.data : error.message);
// //         }
// //     };

// //     return (
// //         <form onSubmit={handleSubmit}>
// //             <div>
// //                 <label>Name:</label>
// //                 <input type="text" name="name" value={formData.name} onChange={handleChange} required />
// //             </div>
// //             <div>
// //                 <label>Gender:</label>
// //                 <input type="text" name="gender" value={formData.gender} onChange={handleChange} required />
// //             </div>
// //             <div>
// //                 <label>Email:</label>
// //                 <input type="email" name="email" value={formData.email} onChange={handleChange} required />
// //             </div>
// //             <div>
// //                 <label>Date of Birth:</label>
// //                 <input type="date" name="dob" value={formData.dob} onChange={handleChange} required />
// //             </div>
// //             <div>
// //                 <label>Mobile Number:</label>
// //                 <input type="text" name="mobileNumber" value={formData.mobileNumber} onChange={handleChange} required />
// //             </div>
// //             <div>
// //                 <label>Given ID:</label>
// //                 <input type="text" name="givenId" value={formData.givenId} onChange={handleChange} required />
// //             </div>
// //             <button type="submit">Register</button>
// //         </form>
// //     );
// // };

// // export default RegistrationForm;

// import React, { useState } from 'react';
// import axios from 'axios';

// const RegistrationForm = () => {
//     const [formData, setFormData] = useState({
//         Name: '',
//         Gender: '',
//         Email: '',
//         DateOfBirth: '',
//         MobileNumber: '',
//         IssuedID: ''
//     });

//     const handleChange = (e) => {
//         setFormData({
//             ...formData,
//             [e.target.name]: e.target.value
//         });
//     };

//     const handleSubmit = async (e) => {
//         e.preventDefault();
//         try {
//             const response = await axios.post('/api/auth/register', formData);
//             console.log('User registered successfully:', response.data);
//         } catch (error) {
//             console.error('Error registering user:', error.response ? error.response.data : error.message);
//         }
//     };

//     return (
//         <form onSubmit={handleSubmit}>
//             <div>
//                 <label>Name:</label>
//                 <input type="text" name="Name" value={formData.Name} onChange={handleChange} required />
//             </div>
//             <div>
//                 <label>Gender:</label>
//                 <input type="text" name="Gender" value={formData.Gender} onChange={handleChange} required />
//             </div>
//             <div>
//                 <label>Email:</label>
//                 <input type="email" name="Email" value={formData.Email} onChange={handleChange} required />
//             </div>
//             <div>
//                 <label>Date of Birth:</label>
//                 <input type="date" name="DateOfBirth" value={formData.DateOfBirth} onChange={handleChange} required />
//             </div>
//             <div>
//                 <label>Mobile Number:</label>
//                 <input type="text" name="MobileNumber" value={formData.MobileNumber} onChange={handleChange} required />
//             </div>
//             <div>
//                 <label>Issued ID:</label>
//                 <input type="text" name="IssuedID" value={formData.IssuedID} onChange={handleChange} required />
//             </div>
//             <button type="submit">Register</button>
//         </form>
//     );
// };

// export default RegistrationForm;


import React, { useState } from 'react';
import axios from 'axios';
import './Register.css'; // Import the CSS file

const RegistrationForm = () => {
    const [formData, setFormData] = useState({
        Name: '',
        Gender: '',
        Email: '',
        DateOfBirth: '',
        MobileNumber: '',
        IssuedID: ''
    });

    const handleChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const response = await axios.post('/api/auth/register', formData);
            console.log('User registered successfully:', response.data);
        } catch (error) {
            console.error('Error registering user:', error.response ? error.response.data : error.message);
        }
    };

    return (
        <form className="registration-form" onSubmit={handleSubmit}>
            <div className="form-group">
                <label htmlFor="Name">Name:</label>
                <input type="text" id="Name" name="Name" value={formData.Name} onChange={handleChange} required />
            </div>
            <div className="form-group">
                <label htmlFor="Gender">Gender:</label>
                <input type="text" id="Gender" name="Gender" value={formData.Gender} onChange={handleChange} required />
            </div>
            <div className="form-group">
                <label htmlFor="Email">Email:</label>
                <input type="email" id="Email" name="Email" value={formData.Email} onChange={handleChange} required />
            </div>
            <div className="form-group">
                <label htmlFor="DateOfBirth">Date of Birth:</label>
                <input type="date" id="DateOfBirth" name="DateOfBirth" value={formData.DateOfBirth} onChange={handleChange} required />
            </div>
            <div className="form-group">
                <label htmlFor="MobileNumber">Mobile Number:</label>
                <input type="text" id="MobileNumber" name="MobileNumber" value={formData.MobileNumber} onChange={handleChange} required />
            </div>
            <div className="form-group">
                <label htmlFor="IssuedID">Issued ID:</label>
                <input type="text" id="IssuedID" name="IssuedID" value={formData.IssuedID} onChange={handleChange} required />
            </div>
            <button className="submit-button" type="submit">Register</button>
        </form>
    );
};

export default RegistrationForm;