// // import React from 'react'
// // import RegistrationForm from './components/Register/Register'
// // import Login from './components/login/Login'
// // import { Route, Routes } from 'react-router-dom'

// // export default function AllRoutes() {
// //   return ( 
// //     <Routes>
// //               <Route path="/register" element={<RegistrationForm  />} />
// //               <Route path="/login" element={<Login/>} />


// //     </Routes>
// //    )}

// // AllRoutes.jsx

// import React from 'react';
// import { Route, Routes } from 'react-router-dom';
// import RegistrationForm from './components/Register/Register';
// import Login from './components/login/Login';

// export default function AllRoutes({ isLoggedIn, onLogin }) {
//   return ( 
//     <Routes>
//       <Route path="/register" element={<RegistrationForm />} />
//       <Route 
//         path="/login" 
//         element={<Login isLoggedIn={isLoggedIn} onLogin={onLogin} />} 
//       />
//     </Routes>
//   );
// }
// AllRoutes.jsx

import React from 'react';
import { Route, Routes } from 'react-router-dom';
import RegistrationForm from './components/Register/Register';
import Login from './components/login/Login';

export default function AllRoutes({ isLoggedIn, onLogin }) {
  return ( 
    <Routes>
      <Route path="/register" element={<RegistrationForm />} />
      <Route 
        path="/login" 
        element={<Login isLoggedIn={isLoggedIn} onLogin={onLogin} />} 
      />
    </Routes>
  );
}

