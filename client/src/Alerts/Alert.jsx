// Alert.js

import React, { useEffect } from 'react';
import { FaCheckCircle } from 'react-icons/fa';
import './Alert.css';

const Alert = ({ message, type, onClose }) => {
    useEffect(() => {
        const timer = setTimeout(() => {
            onClose();
        }, 10000); // Automatically close after 10 seconds

        // Event listener to close alert when clicked outside
        document.addEventListener('click', handleClickOutside);

        return () => {
            clearTimeout(timer);
            document.removeEventListener('click', handleClickOutside);
        };
    }, [onClose]);

    const handleClickOutside = (event) => {
        if (!event.target.closest('.alert')) {
            onClose();
        }
    };

    return (
        <div className={`alert ${type}`}>
            <FaCheckCircle className="icon" />
            <span className="message">{message}</span>
        </div>
    );
};

export default Alert;
