// // // require(dotenv).config()


// // // const express = require('express');
// // // const mongoose = require('mongoose');
// // // const userRoutes = require('../test/Routes/userRoutes');
// // // const authRoutes = require('../test/Routes/authRoutes');
// // // const app = express();

// // // const url = 'mongodb://127.0.0.1:27017/ResumeClients';

// // // app.use(express.json());
// // // app.use('/api/users', userRoutes);
// // // app.use('/api/auth', authRoutes);

// // // mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true })
// // //     .then(() => {
// // //         console.log('Connected to MongoDB');
// // //         app.listen(4000, () => {
// // //             console.log("Server is Running on port 4000");
// // //         });
// // //     })
// // //     .catch(err => {
// // //         console.error('Error connecting to MongoDB', err);
// // //     });

// // // app.get('/', (req, res) => {
// // //     res.send("Welcome to home from Express");
// // // });

// // // app.get('/about', (req, res) => {
// // //     res.send("This is about page");
// // // });

// // require('dotenv').config(); // Load environment variables from .env file

// // const express = require('express');
// // const mongoose = require('mongoose');
// // const userRoutes = require('../test/Routes/userRoutes');
// // const authRoutes = require('../test/Routes/authRoutes');
// // const loginRoutes =  require('../test/Routes/loginRoutes');
// // const app = express();

// // const url = 'mongodb://127.0.0.1:27017/ResumeClients';

// // app.use(express.json());
// // app.use('/api/users', userRoutes);
// // app.use('/api/auth', authRoutes);
// // app.use('/api/login', loginRoutes)

// // mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true })
// //     .then(() => {
// //         console.log('Connected to MongoDB');
// //         app.listen(4000, () => {
// //             console.log("Server is Running on port 4000");
// //         });
// //     })
// //     .catch(err => {
// //         console.error('Error connecting to MongoDB', err);
// //     });

// // app.get('/', (req, res) => {
// //     res.send("Welcome to home from Express");
// // });

// // app.get('/about', (req, res) => {
// //     res.send("This is about page");
// // });


// require('dotenv').config(); // Load environment variables from .env file

// const express = require('express');
// const mongoose = require('mongoose');
// const userRoutes = require('../test/Routes/userRoutes');
// const authRoutes = require('../test/Routes/authRoutes');
// const loginRoutes = require('../test/Routes/loginRoutes');
// const app = express();

// const url = 'mongodb://127.0.0.1:27017/ResumeClients';

// app.use(express.json());
// app.use('/api/users', userRoutes);
// app.use('/api/auth', authRoutes);
// app.use('/api/login', loginRoutes); // Add loginRoutes to handle /api/login requests

// mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true })
//     .then(() => {
//         console.log('Connected to MongoDB');
//         app.listen(4000, () => {
//             console.log("Server is Running on port 4000");
//         });
//     })
//     .catch(err => {
//         console.error('Error connecting to MongoDB', err);
//     });

// app.get('/', (req, res) => {
//     res.send("Welcome to home from Express");
// });

// app.get('/about', (req, res) => {
//     res.send("This is about page");
// });

require('dotenv').config(); // Load environment variables from .env file

const express = require('express');
const mongoose = require('mongoose');
const userRoutes = require('../Server/Routes/userRoutes');
const authRoutes = require('../Server/Routes/authRoutes');
const loginRoutes = require('../Server/Routes/loginRoutes');
const app = express();

const url = 'mongodb://127.0.0.1:27017/ResumeClients';

app.use(express.json());
app.use('/api/users', userRoutes);
app.use('/api/auth', authRoutes);
app.use('/api/login', loginRoutes); // Add loginRoutes to handle /api/login requests

mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => {
        console.log('Connected to MongoDB');
        app.listen(4000, () => {
            console.log("Server is Running on port 4000");
        });
    })
    .catch(err => {
        console.error('Error connecting to MongoDB', err);
    });

app.get('/', (req, res) => {
    res.send("Welcome to home from Express");
});

app.get('/about', (req, res) => {
    res.send("This is about page");
});
